DEFINE TABLE liveuser SCHEMAFULL;
DEFINE FIELD name ON liveuser TYPE string;
DEFINE FIELD lastname ON liveuser TYPE string;
DEFINE FIELD email ON liveuser TYPE string;
DEFINE INDEX email ON TABLE liveuser FIELDS email UNIQUE;

DEFINE TABLE tweet SCHEMAFULL;
DEFINE FIELD payload ON tweet TYPE string;
DEFINE FIELD timestamp ON tweet TYPE datetime;

DEFINE TABLE campaign SCHEMAFULL;
DEFINE FIELD phrase ON campaign TYPE string;
DEFINE FIELD startDate ON campaign TYPE datetime;
DEFINE FIELD endDate ON campaign TYPE datetime;
DEFINE FIELD points ON campaign TYPE int;

INSERT INTO liveuser {
    name: 'Name',
    lastname: 'Lastname',
    email: 'email@email.com'
};

INSERT INTO campaign {
    phrase: 'Phrase',
    startDate: "2024-04-20T16:55:12.731586Z",
    endDate: "2024-05-20T16:55:12.731586Z"
};

INSERT INTO tweet {
    payload: 'payload',
    timestamp: "2024-04-20T16:55:12.731586Z"
};

-- Criar aresta de liveuser para tweet
RELATE liveuser:yimejtqzxzxnqmhq63dw->wrote->tweet:mbdf4miv18cwlur894h6;

-- Criar aresta de tweet para campanign
RELATE tweet:mbdf4miv18cwlur894h6->scored->campaign:9mwmg582d6127w0ndwxk
    CONTENT {
        points: 10,
		timestatmp: time::now()
	};

-- Add a small synopsis composed of the table name and article ID
UPDATE scored SET score = 10, WHERE id=scored:i4t7rtk1ftzz4ugj348n;

-- Selecionar todas as campanign que possuem relação com tweet
SELECT ->scored.out.* AS scores FROM tweet;


-- Selecionar todas as arestas presentes em wrote
SELECT ->wrote.out FROM liveuser:yimejtqzxzxnqmhq63dw
select ->wrote.out as res FROM liveuser;

-- Select da aresta de wrote
SELECT * FROM wrote WHERE in=liveuser:yimejtqzxzxnqmhq63dw;

-- Criar arestas de tweet para campaign
RELATE tweet:mbdf4miv18cwlur894h6->scored->campaign:gxoz2tittwjpls3kqtlu;

-- Deletar a aresta
DELETE liveuser:yimejtqzxzxnqmhq63dw->wrote WHERE out=tweet:mbdf4miv18cwlur894h6;

-- Select da aresta
SELECT * FROM wrote;

-- Informações do banco de dados
INFO FOR DB;

-- Informações da tabela
INFO FOR TABLE liveuser;

-- DROP table
REMOVE table wrote;

-- Seleciona todos os payloads dos Tweets relacionados a um liveuser
SELECT ->wrote->tweet.payload AS friends FROM liveuser:5gvpl2zahvr4rknzp73b;

-- Selecionar todos os Tweets relacionados a um liveuser
SELECT ->wrote->tweet.* AS tweets FROM liveuser:5gvpl2zahvr4rknzp73b;


-- Query com percorrimento de caminho
SELECT ->wrote->tweet->scored.out.* FROM liveuser WHERE id=liveuser:yimejtqzxzxnqmhq63dw;

SELECT ->wrote->tweet->scored.out.* AS campaigns, ->wrote->tweet->scored.in.* AS tweets FROM liveuser WHERE id=liveuser:yimejtqzxzxnqmhq63dw;


-- Todos os usuários que participaram de uma campanha
select <-participated.in.* FROM campaign WHERE id=campaign:9mwmg582d6127w0ndwxk;