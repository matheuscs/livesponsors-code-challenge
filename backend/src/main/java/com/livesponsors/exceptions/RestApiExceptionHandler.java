package com.livesponsors.exceptions;

import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.net.URI;
import java.time.Instant;

@RestControllerAdvice
public class RestApiExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({BusinessException.class})
    public ProblemDetail handleBusinessException(@NotNull BusinessException ex) {
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(
            HttpStatus.BAD_REQUEST,
            ex.getLocalizedMessage()
        );
        problemDetail.setType(URI.create(ex.getClass().getSimpleName()));
        problemDetail.setTitle("An error has occurred");
        problemDetail.setDetail(ex.getMessage());
        problemDetail.setProperty("offsetDateTime", Instant.now());

        return problemDetail;
    }

}