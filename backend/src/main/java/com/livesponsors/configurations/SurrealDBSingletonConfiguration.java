package com.livesponsors.configurations;

import com.surrealdb.connection.SurrealWebSocketConnection;
import com.surrealdb.driver.SyncSurrealDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SurrealDBSingletonConfiguration {

    @Value("${application.vars.surreal-db-host}")
    private static String surrealDbHost = "0.0.0.0";

    @Value("${application.vars.surreal-db-port}")
    private static Integer surrealDbPort = 8000;

    @Value("${application.vars.surreal-db-username}")
    private static String surrealDbUsername = "root";

    @Value("${application.vars.surreal-db-password}")
    private static String surrealDbPassword = "root";

    @Value("${application.vars.surreal-db-namespace}")
    private static String surrealDdNamespace = "test";

    @Value("${application.vars.surreal-db-database}")
    private static String surrealDbDatabase = "test";

    private static SyncSurrealDriver driver = null;

    private SurrealDBSingletonConfiguration(){}

    public static SyncSurrealDriver getDriver() {
        if(driver == null){
            SurrealWebSocketConnection conn = new SurrealWebSocketConnection(
                surrealDbHost,
                surrealDbPort,
                false
            );
            conn.connect(5);
            driver = new SyncSurrealDriver(conn);
            driver.signIn(surrealDbUsername, surrealDbPassword);
            driver.use(surrealDdNamespace, surrealDbDatabase);
        }

        return driver;
    }

}