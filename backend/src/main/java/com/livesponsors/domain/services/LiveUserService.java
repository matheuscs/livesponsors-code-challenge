package com.livesponsors.domain.services;

import com.livesponsors.domain.entities.LiveUser;
import com.livesponsors.domain.repositories.LiveUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class LiveUserService {

    private final LiveUserRepository liveUserRepository;

    @Autowired
    public LiveUserService(LiveUserRepository liveUserRepository) {
        this.liveUserRepository = liveUserRepository;
    }

    public LiveUser findById(String id) {
        return liveUserRepository.findById(id);
    }

    public Long findPointsById(String id) {
        return liveUserRepository.findPointsById(id);
    }

    public List<Object> findByCampaignId(String id) {
        return liveUserRepository.findByCampaignId(id);
    }

    @Transactional(rollbackFor = {Exception.class})
    public LiveUser save(LiveUser liveUser) {
        return liveUserRepository.save(liveUser);
    }

    @Transactional(rollbackFor = {Exception.class})
    public LiveUser update(String id, LiveUser liveUser) {
        return liveUserRepository.update(id, liveUser);
    }

}