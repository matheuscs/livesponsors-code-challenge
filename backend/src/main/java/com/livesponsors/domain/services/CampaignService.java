package com.livesponsors.domain.services;

import com.livesponsors.domain.entities.Campaign;
import com.livesponsors.domain.helpers.WorkflowClientProvider;
import com.livesponsors.domain.repositories.CampaignRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.List;

@Service
public class CampaignService {

    private final CampaignRepository campaignRepository;

    private final WorkflowClientProvider workflowClientProvider;

    @Autowired
    public CampaignService(
        CampaignRepository campaignRepository,
        WorkflowClientProvider workflowClientProvider
    ) {
        this.campaignRepository = campaignRepository;
        this.workflowClientProvider = workflowClientProvider;
    }

    public Campaign findById(String id) {
        return campaignRepository.findById(id);
    }

    @Transactional(rollbackFor = {Exception.class})
    public Campaign save(Campaign campaign) {
        return campaignRepository.save(campaign);
    }

    @Transactional(rollbackFor = {Exception.class})
    public Campaign update(String campaignId, Campaign campaign) {
        campaign = campaignRepository.update(campaignId, campaign);
        workflowClientProvider.syncUpdatedCampaignWorkflowProvider(campaignId);
        return campaign;
    }

    public List<Campaign> findByCurrentDateBetweenStartDateAndEndDate() {
        return campaignRepository.findByCurrentDateBetweenStartDateAndEndDate(OffsetDateTime.now());
    }

}