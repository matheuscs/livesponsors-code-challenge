package com.livesponsors.domain.services;

import com.livesponsors.api.requests.TweetPostRequest;
import com.livesponsors.api.requests.TweetPutRequest;
import com.livesponsors.domain.entities.Tweet;
import com.livesponsors.domain.repositories.CampaignRepository;
import com.livesponsors.domain.repositories.RelationProviderRepository;
import com.livesponsors.domain.repositories.TweetRepository;
import com.livesponsors.exceptions.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.List;

@Service
public class TweetService {

    private final TweetRepository tweetRepository;

    private final LiveUserService liveUserService;

    private final CampaignRepository campaignRepository;

    private final RelationProviderRepository relationProviderRepository;

    @Value("${application.vars.increment-points}")
    private Long incrementPoints = 0L;

    @Autowired
    public TweetService(
        TweetRepository tweetRepository,
        LiveUserService liveUserService,
        RelationProviderRepository relationProviderRepository,
        CampaignRepository campaignRepository
    ) {
        this.tweetRepository = tweetRepository;
        this.liveUserService = liveUserService;
        this.relationProviderRepository = relationProviderRepository;
        this.campaignRepository = campaignRepository;
    }

    public Tweet findById(String id) {
        return tweetRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Entity not found"));
    }

    public List<Tweet> findByLiveUserId(String id) {
        return tweetRepository.findByLiveUserId(id);
    }

    @Transactional(rollbackFor = {Exception.class})
    public Tweet save(TweetPostRequest tweet) {
        var tweetSave = tweetRepository.save(
            Tweet.builder()
                .payload(tweet.getPayload())
                .timestamp(OffsetDateTime.now().toString())
                .build()
        );

        createAllTweetPostedRelations(tweet.getLiveUserId(), tweetSave);

        return tweetSave;
    }

    public Tweet update(String id, TweetPutRequest tweet) {
        return tweetRepository.update(
            id,
            Tweet.builder()
                .id(id)
                .payload(tweet.getPayload())
                .timestamp(OffsetDateTime.now().toString())
                .build());
    }

    public void createAllTweetPostedRelations(String liveUserId, Tweet tweet) {
        var liveUser = liveUserService.findById(liveUserId);

        var activeCampaign = campaignRepository.findByCurrentDateBetweenStartDateAndEndDate(
            OffsetDateTime.now()
        );

        relationProviderRepository.buildLiveUserWroteTweetRelation(liveUser.getId(), tweet.getId());
        activeCampaign.forEach(campaign -> {
            if (tweet.getPayload().toLowerCase().contains(campaign.getPhrase().toLowerCase())) {
                relationProviderRepository.buildLiveUserParticipatedCampaignRelation(
                    liveUser.getId(),
                    campaign.getId(),
                    incrementPoints
                );
                relationProviderRepository.buildTweetScoredCampaignRelation(
                    tweet.getId(),
                    campaign.getId()
                );
            }
        });
    }

    public void executeRollBackUpdatedCampaign(String campaignId){
        var campaign = campaignRepository.findById(campaignId);

        campaignRepository.removeAllLiveUserParticipatedCampaign(campaignId);
        campaignRepository.removeAllTweetScoredCampaign(campaignId);

        campaignRepository.findAllTweetPostedWhenCampaignHasBeenActive(campaign).forEach(tweet -> {
            var relatedLiveUser = campaignRepository.findLiveUserByTweetId(tweet.getId());
            this.createAllTweetPostedRelations(relatedLiveUser.getId(), tweet);
        });
    }

}