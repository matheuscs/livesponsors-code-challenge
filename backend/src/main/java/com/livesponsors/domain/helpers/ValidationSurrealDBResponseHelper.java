package com.livesponsors.domain.helpers;

import com.surrealdb.driver.model.QueryResult;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ValidationSurrealDBResponseHelper<T> {

    ValidationSurrealDBResponseHelper(){}

    public T validateQueryResult(List<QueryResult<T>> t) {
        if(t.isEmpty() ) return null;
        if(t.get(0).getResult().isEmpty()) return null;
        return t.get(0).getResult().get(0);
    }

    public T validateList(List<T> t) {
        if(t.isEmpty() ) return null;
        return t.get(0);
    }

}
