package com.livesponsors.domain.helpers;

import com.livesponsors.domain.workflows.SyncUpdatedCampaignWorkflow;
import com.livesponsors.domain.workflows.impl.SyncUpdatedCampaignActivityImpl;
import com.livesponsors.domain.workflows.impl.SyncUpdatedCampaignWorkflowImpl;
import com.livesponsors.domain.services.TweetService;
import io.temporal.client.WorkflowClient;
import io.temporal.client.WorkflowOptions;
import io.temporal.serviceclient.WorkflowServiceStubs;
import io.temporal.worker.Worker;
import io.temporal.worker.WorkerFactory;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@NoArgsConstructor
public class WorkflowClientProvider {

    private static final String TASK_QUEUE_SYNC = "TASK_QUEUE_SYNC";

    private TweetService tweetService;

    @Autowired
    WorkflowClientProvider(TweetService tweetService) {
        this.tweetService = tweetService;
    }

    public void syncUpdatedCampaignWorkflowProvider(String campaignId) {
        WorkflowServiceStubs service = WorkflowServiceStubs.newInstance();
        WorkflowClient client = WorkflowClient.newInstance(service);
        WorkerFactory factory = WorkerFactory.newInstance(client);
        Worker worker = factory.newWorker(TASK_QUEUE_SYNC);

        worker.registerWorkflowImplementationTypes(SyncUpdatedCampaignWorkflowImpl.class);
        worker.registerActivitiesImplementations(new SyncUpdatedCampaignActivityImpl(tweetService));

        factory.start();

        WorkflowOptions options = WorkflowOptions.newBuilder().setTaskQueue(TASK_QUEUE_SYNC).build();
        SyncUpdatedCampaignWorkflow workflow = client.newWorkflowStub(SyncUpdatedCampaignWorkflow.class, options);
        workflow.syncUpdatedCampaignWorkflow(campaignId);
    }

}