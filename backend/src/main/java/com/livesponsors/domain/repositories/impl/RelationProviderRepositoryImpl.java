package com.livesponsors.domain.repositories.impl;

import com.livesponsors.configurations.SurrealDBSingletonConfiguration;
import com.livesponsors.domain.repositories.RelationProviderRepository;
import com.surrealdb.driver.SyncSurrealDriver;
import com.surrealdb.driver.model.QueryResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class RelationProviderRepositoryImpl implements RelationProviderRepository {

    private final SyncSurrealDriver driver;

    @Autowired
    public RelationProviderRepositoryImpl() {
        this.driver = SurrealDBSingletonConfiguration.getDriver();
    }

    @Override
    public void buildLiveUserWroteTweetRelation(String liveUserIdIn, String tweetIdOut) {
        List<QueryResult<Object>> wrote = driver.query(
            """
                SELECT id AS total FROM wrote
                WHERE in=$liveUserIdIn AND out=$tweetIdOut;
                """,
            Map.of(
                "liveUserIdIn", liveUserIdIn,
                "tweetIdOut", tweetIdOut
            ),
            Object.class
        );

        if (wrote.get(0).getResult().isEmpty()) {
            driver.query(
                """
                    RELATE $liveUserIdIn->wrote->$tweetIdOut;
                    """,
                Map.of(
                    "liveUserIdIn", liveUserIdIn,
                    "tweetIdOut", tweetIdOut
                ),
                Object.class
            );
        }
    }

    @Override
    public void buildTweetScoredCampaignRelation(String tweetIdIn, String campaignIdOut) {
        List<QueryResult<Object>> wrote = driver.query(
            """
                SELECT id AS total FROM scored
                WHERE in=$tweetIdIn AND out=$campaignIdOut;
                """,
            Map.of(
                "tweetIdIn", tweetIdIn,
                "campaignIdOut", campaignIdOut
            ),
            Object.class
        );

        if (wrote.get(0).getResult().isEmpty()) {
            driver.query(
                """
                    RELATE $tweetIdIn->scored->$campaignIdOut
                        CONTENT {
                            timestamp: time::now()
                        };
                    """,
                Map.of(
                    "tweetIdIn", tweetIdIn,
                    "campaignIdOut", campaignIdOut
                ),
                Object.class
            );
        }
    }

    @Override
    public void buildLiveUserParticipatedCampaignRelation(String liveUserIdIn, String campaignIdOut, Long points) {
        List<QueryResult<Object>> wrote = driver.query(
            """
                SELECT id AS total FROM participated
                WHERE in=$liveUserIdIn AND out=$campaignIdOut;
                """,
            Map.of(
                "liveUserIdIn", liveUserIdIn,
                "campaignIdOut", campaignIdOut
            ),
            Object.class
        );

        if (wrote.get(0).getResult().isEmpty()) {
            driver.query(
                """
                    RELATE $liveUserIdIn->participated->$campaignIdOut
                        CONTENT {
                            points: $points,
                            timestamp: time::now()
                        };
                    """,
                Map.of(
                    "liveUserIdIn", liveUserIdIn,
                    "campaignIdOut", campaignIdOut,
                    "points", points.toString()
                ),
                Object.class
            );
        }
    }

}
