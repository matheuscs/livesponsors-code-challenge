package com.livesponsors.domain.repositories;

import com.livesponsors.domain.entities.Campaign;
import com.livesponsors.domain.entities.LiveUser;
import com.livesponsors.domain.entities.Tweet;
import org.springframework.stereotype.Repository;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Set;

@Repository
public interface CampaignRepository {

    List<Campaign> findByCurrentDateBetweenStartDateAndEndDate(OffsetDateTime offSetDateTimeNow);

    Campaign findById(String id);

    Campaign save(Campaign campaign);

    Campaign update(String id, Campaign campaign);

    Set<Tweet> findAllTweetPostedWhenCampaignHasBeenActive(Campaign campaign);

    void removeAllLiveUserParticipatedCampaign(String campaignId);

    void removeAllTweetScoredCampaign(String campaignId);

    LiveUser findLiveUserByTweetId(String tweetId);

}