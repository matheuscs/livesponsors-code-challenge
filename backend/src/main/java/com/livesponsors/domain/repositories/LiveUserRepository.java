package com.livesponsors.domain.repositories;

import com.livesponsors.domain.entities.LiveUser;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LiveUserRepository {

    LiveUser findById(String id);

    Long findPointsById(String id);
    List<Object> findByCampaignId(String campaignId);

    LiveUser save(LiveUser liveUser);

    LiveUser update(String id, LiveUser liveUser);


}