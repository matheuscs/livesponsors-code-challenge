package com.livesponsors.domain.repositories.impl;

import com.livesponsors.configurations.SurrealDBSingletonConfiguration;
import com.livesponsors.domain.entities.Tweet;
import com.livesponsors.domain.helpers.ValidationSurrealDBResponseHelper;
import com.livesponsors.domain.repositories.TweetRepository;
import com.surrealdb.driver.SyncSurrealDriver;
import com.surrealdb.driver.model.QueryResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public class TweetRepositoryImpl implements TweetRepository {

    private final ValidationSurrealDBResponseHelper<Tweet> validationSurrealDBResponseHelper;

    private final SyncSurrealDriver driver;

    @Autowired
    public TweetRepositoryImpl(
        ValidationSurrealDBResponseHelper<Tweet> validationSurrealDBResponseHelper
    ) {
        this.driver = SurrealDBSingletonConfiguration.getDriver();
        this.validationSurrealDBResponseHelper = validationSurrealDBResponseHelper;
    }

    @Override
    public List<Tweet> findByLiveUserId(String liveUserId) {
        List<QueryResult<Object>> query = driver.query(
            "SELECT ->wrote->tweet.* AS tweets FROM $liveUserId;",
            Map.of("liveUserId",  liveUserId),
            Object.class
        );

        return (List) query.get(0).getResult();
    }

    @Override
    public Tweet save(Tweet tweet) {
        return driver.create("tweet", tweet);
    }

    @Override
    public Tweet update(String id, Tweet tweet) {
        return validationSurrealDBResponseHelper.validateList(
            driver.update(id, tweet)
        );
    }

    @Override
    public Optional<Tweet> findById(String id) {
        List<QueryResult<Tweet>> query = driver.query(
            "SELECT * FROM tweet WHERE id=$id;",
            Map.of("id",  id),
            Tweet.class
        );

        return Optional.of(validationSurrealDBResponseHelper.validateQueryResult(query));
    }

}