package com.livesponsors.domain.repositories;

import com.livesponsors.domain.entities.Tweet;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TweetRepository {

    List<Tweet> findByLiveUserId(String id);

    Tweet save(Tweet tweet);

    Tweet update(String id, Tweet tweet);

    Optional<Tweet> findById(String id);

}