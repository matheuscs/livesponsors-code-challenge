package com.livesponsors.domain.repositories.impl;

import com.livesponsors.configurations.SurrealDBSingletonConfiguration;
import com.livesponsors.domain.entities.LiveUser;
import com.livesponsors.domain.helpers.ValidationSurrealDBResponseHelper;
import com.livesponsors.domain.repositories.LiveUserRepository;
import com.surrealdb.driver.SyncSurrealDriver;
import com.surrealdb.driver.model.QueryResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Repository
public class LiveUserRepositoryImpl implements LiveUserRepository {

    @Value("${application.vars.increment-points}")
    private Long incrementPoints = 0L;

    private final ValidationSurrealDBResponseHelper<LiveUser> validationSurrealDBResponseHelper;

    private final SyncSurrealDriver driver;

    @Autowired
    public LiveUserRepositoryImpl(
        ValidationSurrealDBResponseHelper<LiveUser> validationSurrealDBResponseHelper
    ) {
        this.driver = SurrealDBSingletonConfiguration.getDriver();
        this.validationSurrealDBResponseHelper = validationSurrealDBResponseHelper;
    }

    @Override
    public LiveUser save(LiveUser liveUser) {
        return driver.create("liveuser", liveUser);
    }

    @Override
    public LiveUser update(String id, LiveUser liveUser) {
        return validationSurrealDBResponseHelper.validateList(
            driver.update(id, liveUser)
        );
    }

    @Override
    public LiveUser findById(String id) {
        List<QueryResult<LiveUser>> query = driver.query(
            """
                SELECT * FROM liveuser WHERE id=$id;
                """,
            Map.of("id",  id),
            LiveUser.class
        );

        return validationSurrealDBResponseHelper.validateQueryResult(query);
    }

    @Override
    public Long findPointsById(String id) {
        List<QueryResult<Object>> query = driver.query(
            """
                SELECT * FROM participated WHERE in=$id;
                """,
            Map.of("id",  id),
            LiveUser.class
        );

        if(query.isEmpty() ) return 0L;
        if(query.get(0).getResult().isEmpty()) return 0L;
        return query.get(0).getResult().size() * incrementPoints;
    }

    @Override
    public List<Object> findByCampaignId(String campaignId) {
        List<QueryResult<Object>> query = driver.query(
            """
                SELECT <-participated.in.* FROM campaign WHERE id=$campaignId;
                """,
            Map.of("campaignId",  campaignId),
            Object.class
        );

        if(query.isEmpty() ) return Collections.emptyList();
        if(query.get(0).getResult().isEmpty()) return Collections.emptyList();
        return query.get(0).getResult();
    }

}
