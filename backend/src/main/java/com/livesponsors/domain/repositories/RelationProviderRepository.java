package com.livesponsors.domain.repositories;

import org.springframework.stereotype.Repository;

@Repository
public interface RelationProviderRepository {

    void buildLiveUserWroteTweetRelation(String liveUserIdIn, String tweetIdOut);

    void buildTweetScoredCampaignRelation(String tweetIdIn, String campaignIdOut);

    void buildLiveUserParticipatedCampaignRelation(String tweetIdIn, String campaignIdOut, Long points);

}