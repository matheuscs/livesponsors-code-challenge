package com.livesponsors.domain.repositories.impl;

import com.google.gson.internal.LinkedTreeMap;
import com.livesponsors.configurations.SurrealDBSingletonConfiguration;
import com.livesponsors.domain.entities.Campaign;
import com.livesponsors.domain.entities.LiveUser;
import com.livesponsors.domain.entities.Tweet;
import com.livesponsors.domain.helpers.ValidationSurrealDBResponseHelper;
import com.livesponsors.domain.repositories.CampaignRepository;
import com.surrealdb.driver.SyncSurrealDriver;
import com.surrealdb.driver.model.QueryResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Repository
public class CampaignRepositoryImpl implements CampaignRepository {

    private final ValidationSurrealDBResponseHelper<Campaign> validationSurrealDBResponseHelper;

    private final SyncSurrealDriver driver;

    @Autowired
    public CampaignRepositoryImpl(
        ValidationSurrealDBResponseHelper<Campaign> validationSurrealDBResponseHelper
    ) {
        this.driver = SurrealDBSingletonConfiguration.getDriver();
        this.validationSurrealDBResponseHelper = validationSurrealDBResponseHelper;
    }

    @Override
    public Campaign save(Campaign campaign) {
        return driver.create("campaign", campaign);
    }

    @Override
    public Campaign update(String id, Campaign campaign) {
        return validationSurrealDBResponseHelper.validateList(
            driver.update(id, campaign)
        );
    }

    public Set<Tweet> findAllTweetPostedWhenCampaignHasBeenActive(Campaign campaign) {
        List<QueryResult<Object>> query = SurrealDBSingletonConfiguration.getDriver().query(
            """
                SELECT <-wrote.out.* AS wrote FROM tweet
                WHERE timestamp >= $startDate AND timestamp <= $endDate;
                """,
            Map.of(
                "startDate", campaign.getStartDate(),
                "endDate", campaign.getEndDate()
            ),
            Object.class
        );

        var res = query.get(0).getResult().get(0);
        var res2 = (LinkedTreeMap<String, Object>) res;
        var res3 = (List<LinkedTreeMap<String, String>>) res2.get("wrote");

        return res3.stream().map(
            t -> Tweet.builder()
                .id(t.get("id"))
                .payload(t.get("payload"))
                .timestamp(t.get("timestamp")).build()
        ).collect(Collectors.toSet());
    }

    public void removeAllLiveUserParticipatedCampaign(String campaignId) {
        driver.query(
            """
                DELETE participated WHERE out=$campaignId;
                """,
            Map.of("campaignId", campaignId),
            Object.class
        );
    }

    public void removeAllTweetScoredCampaign(String campaignId) {
        driver.query(
            """
                DELETE scored WHERE out=$campaignId;
                """,
            Map.of("campaignId", campaignId),
            Object.class
        );
    }

    public LiveUser findLiveUserByTweetId(String tweetId) {
        List<QueryResult<Object>> query = driver.query(
            """
                SELECT <-wrote.in.* AS wrote FROM tweet WHERE id=$tweetId;
                """,
            Map.of("tweetId", tweetId),
            Object.class
        );

        var res = query.get(0).getResult().get(0);
        var res2 = (LinkedTreeMap<String, Object>) res;
        var res3 = (List<LinkedTreeMap<String, String>>) res2.get("wrote");

        return LiveUser.builder()
            .id(res3.get(0).get("id"))
            .name(res3.get(0).get("name"))
            .lastname(res3.get(0).get("lastname"))
            .email(res3.get(0).get("email")).build();
    }

    public Set<LiveUser> findAllLiveUserParticipatedCampaign(String campaignId) {
        List<QueryResult<Object>> query = SurrealDBSingletonConfiguration.getDriver().query(
            """
                SELECT <-participated.in.* AS participated FROM campaign WHERE id=$campaignId;
                """,
            Map.of("campaignId", campaignId),
            Object.class
        );

        var res = query.get(0).getResult().get(0);
        var res2 = (LinkedTreeMap<String, Object>) res;
        var res3 = (List<LinkedTreeMap<String, String>>) res2.get("participated");

        return res3.stream().map(
            p -> LiveUser.builder()
                .id(p.get("id"))
                .name(p.get("name"))
                .lastname(p.get("lastname"))
                .email(p.get("email")).build()
        ).collect(Collectors.toSet());
    }

    @Override
    public List<Campaign> findByCurrentDateBetweenStartDateAndEndDate(OffsetDateTime offSetDateTimeNow) {
        List<QueryResult<Campaign>> query = driver.query(
            """
                SELECT * FROM campaign
                WHERE startDate <= $offSetDateTime AND endDate >= $offSetDateTime;
                """,
            Map.of("offSetDateTime", OffsetDateTime.now().toString()),
            Campaign.class
        );

        if (query.isEmpty()) return Collections.emptyList();
        if (query.get(0).getResult().isEmpty()) return Collections.emptyList();
        return query.get(0).getResult();
    }

    @Override
    public Campaign findById(String id) {
        List<QueryResult<Campaign>> query = driver.query(
            """
                SELECT * FROM campaign WHERE id = $id;
                """,
            Map.of("id", id),
            Campaign.class
        );

        return validationSurrealDBResponseHelper.validateQueryResult(query);
    }

}
