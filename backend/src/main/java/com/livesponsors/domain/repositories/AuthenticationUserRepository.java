package com.livesponsors.domain.repositories;

import com.livesponsors.domain.entities.AuthenticationUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AuthenticationUserRepository extends JpaRepository<AuthenticationUser, Long> {
    // TODO: JWT authentication
    Optional<AuthenticationUser> findByEmailContainingIgnoreCase(String email);

}