package com.livesponsors.domain.workflows.impl;

import com.livesponsors.domain.workflows.SyncUpdatedCampaignActivity;
import com.livesponsors.domain.services.TweetService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class SyncUpdatedCampaignActivityImpl implements SyncUpdatedCampaignActivity {

    private static final Logger logger = LoggerFactory.getLogger(SyncUpdatedCampaignActivityImpl.class);

    private final TweetService tweetService;

    public SyncUpdatedCampaignActivityImpl(TweetService tweetService){
        this.tweetService = tweetService;
    }

    @Override
    public String syncOldTweetsWithNewCampaignPhraseActivity(String campaignId) {
        tweetService.executeRollBackUpdatedCampaign(campaignId);

        logger.info("syncOldTweetsWithNewCampaignPhraseActivity Executed for: " + campaignId);
        return "syncOldTweetsWithNewCampaignPhraseActivity has been started the execution.";
    }
}
