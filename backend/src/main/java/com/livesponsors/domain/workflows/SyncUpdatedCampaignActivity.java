package com.livesponsors.domain.workflows;

import io.temporal.activity.ActivityInterface;

@ActivityInterface
public interface SyncUpdatedCampaignActivity {
    String syncOldTweetsWithNewCampaignPhraseActivity(String input);
}