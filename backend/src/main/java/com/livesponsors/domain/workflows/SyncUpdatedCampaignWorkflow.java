package com.livesponsors.domain.workflows;

import io.temporal.workflow.WorkflowInterface;
import io.temporal.workflow.WorkflowMethod;

@WorkflowInterface
public interface SyncUpdatedCampaignWorkflow {
    @WorkflowMethod
    void syncUpdatedCampaignWorkflow(String input);
}