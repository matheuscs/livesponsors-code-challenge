package com.livesponsors.domain.workflows.impl;

import com.livesponsors.domain.workflows.SyncUpdatedCampaignActivity;
import com.livesponsors.domain.workflows.SyncUpdatedCampaignWorkflow;
import io.temporal.activity.ActivityOptions;
import io.temporal.workflow.Workflow;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.Duration;

public class SyncUpdatedCampaignWorkflowImpl implements SyncUpdatedCampaignWorkflow {

    private static final Logger logger = LoggerFactory.getLogger(SyncUpdatedCampaignWorkflowImpl.class);

    private final SyncUpdatedCampaignActivity syncOldTweetsWithNewCampaignPhraseActivity = Workflow.newActivityStub(
        SyncUpdatedCampaignActivity.class,
        ActivityOptions.newBuilder()
            .setStartToCloseTimeout(Duration.ofMinutes(1))
            .build()
    );

    @Override
    public void syncUpdatedCampaignWorkflow(String campaignId) {
        logger.info("syncUpdatedCampaignWorkflow: campainId=%s".formatted(campaignId));
        syncOldTweetsWithNewCampaignPhraseActivity.syncOldTweetsWithNewCampaignPhraseActivity(campaignId);
    }

}