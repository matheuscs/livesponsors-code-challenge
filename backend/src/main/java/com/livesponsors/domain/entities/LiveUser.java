package com.livesponsors.domain.entities;

import lombok.*;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LiveUser {
    private String id;
    private String name;
    private String lastname;
    private String email;
}