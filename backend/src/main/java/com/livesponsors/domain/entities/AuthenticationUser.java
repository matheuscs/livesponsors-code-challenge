package com.livesponsors.domain.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import lombok.*;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "AuthenticationUser")
@Table(name = "authentication_user")
public class AuthenticationUser {

    @Id
    @NotEmpty(message = "email required")
    @Email
    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @NotEmpty(message = "password required")
    @Column(name = "password", nullable = false)
    private String password;

    @NotEmpty(message = "role required")
    @Column(name = "role", nullable = false)
    private String role;
}