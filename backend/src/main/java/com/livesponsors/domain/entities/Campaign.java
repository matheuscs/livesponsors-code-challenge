package com.livesponsors.domain.entities;

import lombok.*;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class Campaign {
    private String id;
    private String phrase;
    private String startDate;
    private String endDate;
}