package com.livesponsors.domain.entities;

import lombok.*;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Tweet {
    private String id;
    private String payload;
    private String timestamp;
}