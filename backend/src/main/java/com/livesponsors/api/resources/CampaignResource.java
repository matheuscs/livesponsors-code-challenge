package com.livesponsors.api.resources;

import com.livesponsors.domain.entities.Campaign;
import com.livesponsors.domain.services.CampaignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;

@RestController
@RequestMapping("/campaigns")
public class CampaignResource {

    private final CampaignService campaignService;

    @Autowired
    public CampaignResource(CampaignService campaignService) {
        this.campaignService = campaignService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Campaign> getById(
        @PathVariable(name = "id") String id
    ) {
        return ResponseEntity.ok().body(campaignService.findById(id));
    }

    @PostMapping
    public ResponseEntity<Campaign> postCampaign(
        @RequestBody Campaign campaign
    ) {
        var response = campaignService.save(campaign);
        return ResponseEntity.created(
                ServletUriComponentsBuilder.fromCurrentRequest()
                    .path("/{id}")
                    .buildAndExpand(response.getId())
                    .toUri())
            .body(response);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Campaign> putCampaign(
        @PathVariable(name = "id") String id,
        @RequestBody Campaign campaign
    ) {
        campaignService.update(id, campaign);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/actives")
    public ResponseEntity<List<Campaign>> getById() {
        return ResponseEntity.ok().body(campaignService.findByCurrentDateBetweenStartDateAndEndDate());
    }

}