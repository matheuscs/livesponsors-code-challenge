package com.livesponsors.api.resources;

import com.livesponsors.domain.entities.LiveUser;
import com.livesponsors.domain.services.LiveUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;

@RestController
@RequestMapping("/live-users")
public class LiveUserResource {

    private final LiveUserService liveUserService;

    @Autowired
    public LiveUserResource(LiveUserService liveUserService) {
        this.liveUserService = liveUserService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<LiveUser> getById(@PathVariable(name = "id") String id) {
        return ResponseEntity.ok().body(liveUserService.findById(id));
    }

    @GetMapping("/{id}/points")
    public ResponseEntity<Long> getPointsById(@PathVariable(name = "id") String id) {
        return ResponseEntity.ok().body(liveUserService.findPointsById(id));
    }

    @GetMapping("/campaigns/id/{campaignId}")
    public ResponseEntity<List<Object>> getByCampaignId(@PathVariable(name = "campaignId") String campaignId) {
        return ResponseEntity.ok().body(liveUserService.findByCampaignId(campaignId));
    }

    @PostMapping
    public ResponseEntity<LiveUser> postLiveUser(@RequestBody LiveUser liveUser) {
        var response = liveUserService.save(liveUser);
        return ResponseEntity.created(
            ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(response.getId())
                .toUri())
            .body(response);
    }

    @PutMapping("/{id}")
    public ResponseEntity<LiveUser> postLiveUser(@PathVariable(name = "id") String id, @RequestBody LiveUser liveUser) {
        liveUserService.update(id, liveUser);
        return ResponseEntity.noContent().build();
    }

}