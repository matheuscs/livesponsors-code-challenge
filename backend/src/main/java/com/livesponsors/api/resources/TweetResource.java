package com.livesponsors.api.resources;

import com.livesponsors.api.requests.TweetPostRequest;
import com.livesponsors.api.requests.TweetPutRequest;
import com.livesponsors.domain.entities.Tweet;
import com.livesponsors.domain.services.TweetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;

@RestController
@RequestMapping("/tweets")
public class TweetResource {

    private final TweetService tweetService;

    @Autowired
    public TweetResource(TweetService tweetService) {
        this.tweetService = tweetService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Tweet> getById(
        @PathVariable(name = "id") String id
    ) {
        return ResponseEntity.ok().body(tweetService.findById(id));
    }

    @GetMapping("/live-user/id/{id}")
    public ResponseEntity<List<Tweet>> getByLiveUserId(
        @PathVariable(name = "id") String id
    ) {
        return ResponseEntity.ok().body(tweetService.findByLiveUserId(id));
    }

    @PostMapping
    public ResponseEntity<Tweet> postTweet(
        @RequestBody TweetPostRequest tweet
    ) {
        var response = tweetService.save(tweet);
        return ResponseEntity.created(
                ServletUriComponentsBuilder.fromCurrentRequest()
                    .path("/{id}")
                    .buildAndExpand(response.getId())
                    .toUri())
            .body(response);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Tweet> postTweet(
        @PathVariable(name = "id") String id,
        @RequestBody TweetPutRequest tweet
    ) {
        tweetService.update(id, tweet);
        return ResponseEntity.noContent().build();
    }

}