package com.livesponsors.api.requests;

import lombok.*;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class TweetPutRequest {
    private String id;
    private String payload;
    private String liveUserId;
}
