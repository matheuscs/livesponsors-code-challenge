package com.livesponsors.api.requests;

import lombok.*;

import java.time.OffsetDateTime;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class CampaignPostRequest {
    private Long id;
    private String phrase;
    private OffsetDateTime startDate;
    private OffsetDateTime endDate;
}
