package com.livesponsors.api.responses;

import lombok.*;

import java.time.OffsetDateTime;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class LiveUserGetResponse {
    private Long id;
    private String payload;
    private OffsetDateTime timestamp;
    private Long liveUserId;
}
