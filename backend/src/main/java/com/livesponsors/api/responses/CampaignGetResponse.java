package com.livesponsors.api.responses;

import lombok.*;

import java.time.OffsetDateTime;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class CampaignGetResponse {
    private Long id;
    private String phrase;
    private OffsetDateTime startDate;
    private OffsetDateTime endDate;
}
