package com.livesponsors.api.responses;

import lombok.*;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class TweetGetResponse {

    private String id;
    private String payload;
    private String liveUserId;

}
