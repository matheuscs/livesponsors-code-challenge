package com.livesponsors.api.responses;

import lombok.*;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class TweetPutResponse {
    private String id;
    private String payload;
    private String timestamp;
    private String liveUserId;
}
