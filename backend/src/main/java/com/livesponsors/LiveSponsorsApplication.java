package com.livesponsors;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LiveSponsorsApplication {

	public static void main(String[] args) {
		SpringApplication.run(LiveSponsorsApplication.class, args);
	}

}