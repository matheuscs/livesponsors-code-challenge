package com.livesponsors.domain.services;

import com.livesponsors.domain.entities.LiveUser;
import com.livesponsors.domain.repositories.LiveUserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class LiveUserServiceTest {

    @Mock
    private LiveUserRepository liveUserRepository;

    @InjectMocks
    private LiveUserService liveUserService;

    @Test
    void testFindById() {
        LiveUser mockLiveUser = new LiveUser();
        mockLiveUser.setId("liveuser:8bhr9mqlacrewtmq2skj");

        when(liveUserRepository.findById("liveuser:8bhr9mqlacrewtmq2skj")).thenReturn(mockLiveUser);

        LiveUser result = liveUserService.findById("liveuser:8bhr9mqlacrewtmq2skj");

        assertEquals("liveuser:8bhr9mqlacrewtmq2skj", result.getId());
        verify(liveUserRepository, times(1)).findById("liveuser:8bhr9mqlacrewtmq2skj");
    }

    @Test
    void testFindPointsById() {
        Long points = 10L;

        when(liveUserRepository.findPointsById("liveuser:8bhr9mqlacrewtmq2skj")).thenReturn(points);

        Long result = liveUserService.findPointsById("liveuser:8bhr9mqlacrewtmq2skj");

        assertEquals(points, result);
        verify(liveUserRepository, times(1)).findPointsById("liveuser:8bhr9mqlacrewtmq2skj");
    }

    @Test
    void testFindByCampaignId() {
        Object campaign1 = new Object();
        Object campaign2 = new Object();

        when(liveUserRepository.findByCampaignId("campaign:8bhr9mqlacrewtmq2skj"))
            .thenReturn(Arrays.asList(campaign1, campaign2));

        List<Object> result = liveUserService.findByCampaignId("campaign:8bhr9mqlacrewtmq2skj");

        assertEquals(2, result.size());
        verify(liveUserRepository, times(1)).findByCampaignId("campaign:8bhr9mqlacrewtmq2skj");
    }

    @Test
    void testSave() {
        LiveUser newLiveUser = new LiveUser();
        newLiveUser.setId("liveuser:8bhr9mqlacrewtmq2skj");

        when(liveUserRepository.save(any(LiveUser.class))).thenReturn(newLiveUser);

        LiveUser result = liveUserService.save(newLiveUser);

        assertEquals("liveuser:8bhr9mqlacrewtmq2skj", result.getId());
        verify(liveUserRepository, times(1)).save(any(LiveUser.class));
    }

    @Test
    void testUpdate() {
        LiveUser updatedLiveUser = new LiveUser();
        updatedLiveUser.setId("liveuser:8bhr9mqlacrewtmq2skj");

        when(liveUserRepository.update("liveuser:8bhr9mqlacrewtmq2skj", updatedLiveUser))
            .thenReturn(updatedLiveUser);

        LiveUser result = liveUserService.update("liveuser:8bhr9mqlacrewtmq2skj", updatedLiveUser);

        assertEquals("liveuser:8bhr9mqlacrewtmq2skj", result.getId());
        verify(liveUserRepository, times(1))
            .update("liveuser:8bhr9mqlacrewtmq2skj", updatedLiveUser);
    }
}

