package com.livesponsors.domain.services;

import com.livesponsors.domain.entities.Tweet;
import com.livesponsors.domain.repositories.CampaignRepository;
import com.livesponsors.domain.repositories.RelationProviderRepository;
import com.livesponsors.domain.repositories.TweetRepository;
import com.livesponsors.exceptions.EntityNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TweetServiceTest {

    @Mock
    private TweetRepository tweetRepository;

    @Mock
    private LiveUserService liveUserService;

    @Mock
    private CampaignRepository campaignRepository;

    @Mock
    private RelationProviderRepository relationProviderRepository;

    @InjectMocks
    private TweetService tweetService;

    @Test
    void testFindById_Success() {
        Tweet expectedTweet = Tweet.builder().id("tweet:8bhr9mqlacrewtmq2skj").build();

        when(tweetRepository.findById("tweet:8bhr9mqlacrewtmq2skj")).thenReturn(Optional.of(expectedTweet));

        Tweet result = tweetService.findById("tweet:8bhr9mqlacrewtmq2skj");

        assertEquals("tweet:8bhr9mqlacrewtmq2skj", result.getId());
        verify(tweetRepository, times(1)).findById("tweet:8bhr9mqlacrewtmq2skj");
    }

    @Test
    void testFindById_NotFound() {
        when(tweetRepository.findById("tweet:8bhr9mqlacrewtmq2skj")).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> tweetService.findById("tweet:8bhr9mqlacrewtmq2skj"));
        verify(tweetRepository, times(1)).findById("tweet:8bhr9mqlacrewtmq2skj");
    }

    @Test
    void testFindByLiveUserId() {
        List<Tweet> expectedTweets = Collections.singletonList(
            Tweet.builder().id("tweet:8bhr9mqlacrewtmq2skj").build()
        );

        when(tweetRepository.findByLiveUserId("tweet:8bhr9mqlacrewtmq2skj")).thenReturn(expectedTweets);

        List<Tweet> result = tweetService.findByLiveUserId("tweet:8bhr9mqlacrewtmq2skj");

        assertEquals(expectedTweets.size(), result.size());
        verify(tweetRepository, times(1)).findByLiveUserId("tweet:8bhr9mqlacrewtmq2skj");
    }

}

