package com.livesponsors.domain.services;

import com.livesponsors.domain.entities.Campaign;
import com.livesponsors.domain.helpers.WorkflowClientProvider;
import com.livesponsors.domain.repositories.CampaignRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CampaignServiceTest {

    @Mock
    private CampaignRepository campaignRepository;

    @Mock
    private WorkflowClientProvider workflowClientProvider;

    @InjectMocks
    private CampaignService campaignService;

    @Test
    void testFindById() {
        Campaign mockCampaign = new Campaign();
        mockCampaign.setId("campaign:8bhr9mqlacrewtmq2skj");

        when(campaignRepository.findById("campaign:8bhr9mqlacrewtmq2skj")).thenReturn(mockCampaign);

        Campaign result = campaignService.findById("campaign:8bhr9mqlacrewtmq2skj");

        assertEquals("campaign:8bhr9mqlacrewtmq2skj", result.getId());
        verify(campaignRepository, times(1)).findById("campaign:8bhr9mqlacrewtmq2skj");
    }

    @Test
    void testSave() {
        Campaign newCampaign = new Campaign();
        newCampaign.setId("campaign:8bhr9mqlacrewtmq2skj");

        when(campaignRepository.save(any(Campaign.class))).thenReturn(newCampaign);

        Campaign result = campaignService.save(newCampaign);

        assertEquals("campaign:8bhr9mqlacrewtmq2skj", result.getId());
        verify(campaignRepository, times(1)).save(any(Campaign.class));
    }

    @Test
    void testUpdate() {
        Campaign updatedCampaign = new Campaign();
        updatedCampaign.setId("campaign:8bhr9mqlacrewtmq2skj");

        when(
            campaignRepository.update("campaign:8bhr9mqlacrewtmq2skj", updatedCampaign)
        ).thenReturn(updatedCampaign);

        Campaign result = campaignService.update("campaign:8bhr9mqlacrewtmq2skj", updatedCampaign);

        assertEquals("campaign:8bhr9mqlacrewtmq2skj", result.getId());
        verify(campaignRepository, times(1)).update(
            "campaign:8bhr9mqlacrewtmq2skj", updatedCampaign
        );
        verify(workflowClientProvider, times(1))
            .syncUpdatedCampaignWorkflowProvider("campaign:8bhr9mqlacrewtmq2skj");
    }

    @Test
    void testFindByCurrentDateBetweenStartDateAndEndDate() {
        Campaign campaign1 = new Campaign();
        campaign1.setId("campaign:8bhr9mqlacrewtmq2skj");

        Campaign campaign2 = new Campaign();
        campaign2.setId("campaign:8bhr9mqlacrewtmq2sk2");

        when(campaignRepository.findByCurrentDateBetweenStartDateAndEndDate(any(OffsetDateTime.class))).thenReturn(
            Arrays.asList(campaign1, campaign2)
        );

        List<Campaign> result = campaignService.findByCurrentDateBetweenStartDateAndEndDate();

        assertEquals(2, result.size());
        assertEquals("campaign:8bhr9mqlacrewtmq2skj", result.get(0).getId());
        assertEquals("campaign:8bhr9mqlacrewtmq2sk2", result.get(1).getId());
        verify(campaignRepository, times(1)).findByCurrentDateBetweenStartDateAndEndDate(any(OffsetDateTime.class));
    }
}

