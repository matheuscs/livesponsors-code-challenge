package com.livesponsors.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.livesponsors.domain.entities.LiveUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
class LiveUserResourceIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void testGetById() throws Exception {
        String id = "liveuser:8bhr9mqlacrewtmq2skj";

        mockMvc.perform(get("/live-users/{id}", id))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andDo(MockMvcResultHandlers.print());
    }

    @Test
    void testGetPointsById() throws Exception {
        String id = "liveuser:8bhr9mqlacrewtmq2skj";

        mockMvc.perform(get("/live-users/{id}/points", id))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$").isNumber())
            .andDo(MockMvcResultHandlers.print());
    }

    @Test
    void testGetByCampaignId() throws Exception {
        String campaignId = "compaign:8bhr9mqlacrewtmq2skj";

        mockMvc.perform(get("/live-users/campaigns/id/{campaignId}", campaignId))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andDo(MockMvcResultHandlers.print());
    }

    @Test
    void testPostLiveUser() throws Exception {
        LiveUser liveUser = new LiveUser();
        liveUser.setName("name");
        liveUser.setLastname("lasname");
        liveUser.setEmail(UUID.randomUUID() + "@email.com");

        mockMvc.perform(post("/live-users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(liveUser)))
            .andExpect(status().isCreated())
            .andExpect(header().exists("Location"))
            .andDo(MockMvcResultHandlers.print());
    }

    @Test
    void testPutLiveUser() throws Exception {
        String id = "liveuser:8bhr9mqlacrewtmq2skj";
        LiveUser liveUser = new LiveUser();
        liveUser.setName("name");
        liveUser.setLastname("lasname");
        liveUser.setEmail(UUID.randomUUID() + "@email.com");

        mockMvc.perform(put("/live-users/{id}", id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(liveUser)))
            .andExpect(status().isNoContent())
            .andDo(MockMvcResultHandlers.print());
    }

}