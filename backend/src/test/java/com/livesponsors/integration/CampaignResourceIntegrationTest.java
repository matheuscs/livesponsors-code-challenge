package com.livesponsors.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.livesponsors.domain.entities.Campaign;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
class CampaignResourceIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void testGetById() throws Exception {
        String id = "campaign:8bhr9mqlacrewtmq2skj";

        mockMvc.perform(get("/campaigns/{id}", id))
            .andExpect(status().isOk());
    }

    @Test
    void testPostCampaign() throws Exception {
        Campaign campaign = new Campaign();
        campaign.setPhrase("phrase");
        campaign.setStartDate("2024-01-01T12:00:00+00:00");
        campaign.setEndDate("2024-02-01T12:00:00+00:00");

        mockMvc.perform(post("/campaigns")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(campaign)))
            .andExpect(status().isCreated())
            .andExpect(header().exists("Location"));
    }

    @Test
    void testGetActives() throws Exception {
        mockMvc.perform(get("/campaigns/actives"))
            .andExpect(status().isOk());
    }

}