package com.livesponsors.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.livesponsors.api.requests.TweetPostRequest;
import com.livesponsors.api.requests.TweetPutRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
class TweetResourceIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void testGetByLiveUserId() throws Exception {
        String liveUserId = "liveuser:8bhr9mqlacrewtmq2skj";

        mockMvc.perform(get("/tweets/live-user/id/{id}", liveUserId))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andDo(MockMvcResultHandlers.print());
    }

    @Test
    void testPostTweet() throws Exception {
        TweetPostRequest tweetPostRequest = new TweetPostRequest();
        tweetPostRequest.setPayload("Test tweet");
        tweetPostRequest.setLiveUserId("liveuser:8bhr9mqlacrewtmq2skj");

        mockMvc.perform(post("/tweets")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(tweetPostRequest)))
            .andExpect(status().isCreated())
            .andExpect(header().exists("Location"))
            .andExpect(jsonPath("$.payload").value("Test tweet"))
            .andDo(MockMvcResultHandlers.print());
    }

    @Test
    void testPutTweet() throws Exception {
        String id = "tweet:8bhr9mqlacrewtmq2skj";
        TweetPutRequest tweetPutRequest = new TweetPutRequest();
        tweetPutRequest.setPayload("Updated tweet");

        mockMvc.perform(put("/tweets/{id}", id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(tweetPutRequest)))
            .andExpect(status().isNoContent())
            .andDo(MockMvcResultHandlers.print());
    }

}