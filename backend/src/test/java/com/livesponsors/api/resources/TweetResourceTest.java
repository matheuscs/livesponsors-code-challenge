package com.livesponsors.api.resources;

import com.livesponsors.api.requests.TweetPostRequest;
import com.livesponsors.api.requests.TweetPutRequest;
import com.livesponsors.domain.entities.Tweet;
import com.livesponsors.domain.services.TweetService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@WebMvcTest(TweetResource.class)
class TweetResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TweetService tweetService;

    @Test
    void testGetById() throws Exception {
        Tweet mockTweet = new Tweet();
        mockTweet.setId("tweet:8bhr9mqlacrewtmq2skj");
        mockTweet.setPayload("This is a tweet");

        when(tweetService.findById("tweet:8bhr9mqlacrewtmq2skj")).thenReturn(mockTweet);

        mockMvc.perform(MockMvcRequestBuilders.get("/tweets/tweet:8bhr9mqlacrewtmq2skj"))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").value("tweet:8bhr9mqlacrewtmq2skj"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.payload").value("This is a tweet"));
    }

    @Test
    void testGetByLiveUserId() throws Exception {
        Tweet tweet1 = new Tweet();
        tweet1.setId("tweet:8bhr9mqlacrewtmq2skj");
        tweet1.setPayload("Tweet from user 1");

        Tweet tweet2 = new Tweet();
        tweet2.setId("tweet:8bhr9mqlacrewtmq2sk2");
        tweet2.setPayload("Another tweet from user 1");

        when(tweetService.findByLiveUserId("liveuser:8bhr9mqlacrewtmq2skj")).thenReturn(Arrays.asList(tweet1, tweet2));

        mockMvc.perform(MockMvcRequestBuilders.get("/tweets/live-user/id/liveuser:8bhr9mqlacrewtmq2skj"))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value("tweet:8bhr9mqlacrewtmq2skj"))
            .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value("tweet:8bhr9mqlacrewtmq2sk2"));
    }

    @Test
    void testPostTweet() throws Exception {
        TweetPostRequest tweetRequest = new TweetPostRequest();
        tweetRequest.setPayload("New tweet");
        tweetRequest.setLiveUserId("liveuser:8bhr9mqlacrewtmq2skj");

        Tweet savedTweet = new Tweet();
        savedTweet.setTimestamp("2024-02-20T13:55:12.7315861-03:00");
        savedTweet.setPayload("New tweet");
        savedTweet.setId("tweet:8bhr9mqlacrewtmq2skj");


        when(tweetService.save(any(TweetPostRequest.class))).thenReturn(savedTweet);

        String requestJson = """
            {
                "payload": "New tweet",
                "liveUserId": "liveuser:8bhr9mqlacrewtmq2skj"
            }
            """;

        mockMvc.perform(
                MockMvcRequestBuilders.post("/tweets")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(requestJson)
            )
            .andExpect(MockMvcResultMatchers.status().isCreated())
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").value("tweet:8bhr9mqlacrewtmq2skj"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.payload").value("New tweet"));
    }

    @Test
    void testPutTweet() throws Exception {
        TweetPutRequest tweetPutRequest = new TweetPutRequest();
        tweetPutRequest.setPayload("Updated tweet");
        tweetPutRequest.setId("tweet:8bhr9mqlacrewtmq2skj");
        tweetPutRequest.setLiveUserId("liveuser:8bhr9mqlacrewtmq2skj");

        Tweet updatedTweet = new Tweet();
        updatedTweet.setId("tweet:8bhr9mqlacrewtmq2skj");
        updatedTweet.setPayload("Updated tweet");
        updatedTweet.setTimestamp("2024-04-21T23:49:32.022646400Z");

        when(tweetService.update("tweet:8bhr9mqlacrewtmq2skj", tweetPutRequest)).thenReturn(updatedTweet);

        String requestJson = """
            {
                "id": "tweet:8bhr9mqlacrewtmq2skj",
                "payload": "Updated tweet",
                "liveUserId": "liveuser:8bhr9mqlacrewtmq2skj"
            }
            """;

        mockMvc.perform(
                MockMvcRequestBuilders.put("/tweets/tweet:8bhr9mqlacrewtmq2skj")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(requestJson)
            )
            .andExpect(MockMvcResultMatchers.status().isNoContent());
    }
}
