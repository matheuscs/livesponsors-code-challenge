package com.livesponsors.api.resources;

import com.livesponsors.domain.entities.LiveUser;
import com.livesponsors.domain.services.LiveUserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@WebMvcTest(LiveUserResource.class)
class LiveUserResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private LiveUserService liveUserService;

    @Test
    void testGetById() throws Exception {
        LiveUser mockLiveUser = new LiveUser();
        mockLiveUser.setId("liveuser:8bhr9mqlacrewtmq2skj");
        mockLiveUser.setName("User 1");

        when(liveUserService.findById("liveuser:8bhr9mqlacrewtmq2skj")).thenReturn(mockLiveUser);

        mockMvc.perform(MockMvcRequestBuilders.get("/live-users/liveuser:8bhr9mqlacrewtmq2skj"))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").value("liveuser:8bhr9mqlacrewtmq2skj"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("User 1"));
    }

    @Test
    void testGetPointsById() throws Exception {
        when(liveUserService.findPointsById("liveuser:8bhr9mqlacrewtmq2skj")).thenReturn(100L);

        mockMvc.perform(MockMvcRequestBuilders.get("/live-users/liveuser:8bhr9mqlacrewtmq2skj/points"))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$").value(100L));
    }

    @Test
    void testGetByCampaignId() throws Exception {
        LiveUser user1 = new LiveUser();
        user1.setId("liveuser:8bhr9mqlacrewtmq2skj");
        user1.setName("User 1");

        LiveUser user2 = new LiveUser();
        user2.setId("liveuser:8bhr9mqlacrewtmq2sk2");
        user2.setName("User 2");

        when(liveUserService.findByCampaignId("campaign:8bhr9mqlacrewtmq2skj")).thenReturn(Arrays.asList(user1, user2));

        mockMvc.perform(MockMvcRequestBuilders.get("/live-users/campaigns/id/campaign:8bhr9mqlacrewtmq2skj"))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value("liveuser:8bhr9mqlacrewtmq2skj"))
            .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value("liveuser:8bhr9mqlacrewtmq2sk2"));
    }

    @Test
    void testPostLiveUser() throws Exception {
        LiveUser newUser = new LiveUser();
        newUser.setId("liveuser:8bhr9mqlacrewtmq2skj");
        newUser.setName("name");
        newUser.setLastname("lastname");
        newUser.setEmail("email@email.com");

        when(liveUserService.save(any(LiveUser.class))).thenReturn(newUser);

        String requestJson = """
            {
                "name": "name",
                "lastname": "lastname",
                "email": "email@email.com"
            }
            """;

        mockMvc.perform(
                MockMvcRequestBuilders.post("/live-users")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(requestJson)
            )
            .andExpect(MockMvcResultMatchers.status().isCreated())
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").value("liveuser:8bhr9mqlacrewtmq2skj"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("name"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.lastname").value("lastname"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("email@email.com"));
    }

    @Test
    void testPutLiveUser() throws Exception {
        LiveUser updatedUser = new LiveUser();
        updatedUser.setId("liveuser:8bhr9mqlacrewtmq2skj");
        updatedUser.setName("name up");
        updatedUser.setLastname("lastname up");
        updatedUser.setEmail("emailup@email.com");

        when(liveUserService.update("liveuser:8bhr9mqlacrewtmq2skj", updatedUser)).thenReturn(updatedUser);

        String requestJson = """
            {
                "id": "liveuser:8bhr9mqlacrewtmq2skj",
                "name": "name",
                "lastname": "lastname",
                "email": "email@email.com"
            }
            """;

        mockMvc.perform(
                MockMvcRequestBuilders.put("/live-users/liveuser:8bhr9mqlacrewtmq2skj")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(requestJson)
            )
            .andExpect(MockMvcResultMatchers.status().isNoContent());
    }
}

