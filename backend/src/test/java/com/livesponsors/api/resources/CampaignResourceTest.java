package com.livesponsors.api.resources;

import com.livesponsors.domain.entities.Campaign;
import com.livesponsors.domain.services.CampaignService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@WebMvcTest(CampaignResource.class)
class CampaignResourceTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CampaignService campaignService;

    @Test
    void testGetById() throws Exception {
        Campaign mockCampaign = new Campaign();
        mockCampaign.setId("campaign:8bhr9mqlacrewtmq2skj");
        mockCampaign.setPhrase("Campaign 1");

        when(campaignService.findById("campaign:8bhr9mqlacrewtmq2skj")).thenReturn(mockCampaign);

        mockMvc.perform(MockMvcRequestBuilders.get("/campaigns/campaign:8bhr9mqlacrewtmq2skj"))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").value("campaign:8bhr9mqlacrewtmq2skj"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.phrase").value("Campaign 1"));
    }

    @Test
    void testPostCampaign() throws Exception {
        Campaign newCampaign = new Campaign();
        newCampaign.setId("campaign:8bhr9mqlacrewtmq2skj");
        newCampaign.setPhrase("New Campaign");

        when(campaignService.save(any(Campaign.class))).thenReturn(newCampaign);

        String requestJson = """
            {
                "phrase": "campanha-2",
                "startDate": "2024-02-20T13:55:12.7315861-03:00",
                "endDate": "2024-08-20T13:55:12.7315861-03:00"
            }
            """;

        mockMvc.perform(
                MockMvcRequestBuilders.post("/campaigns")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(requestJson)
            )
            .andExpect(MockMvcResultMatchers.status().isCreated())
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").value("campaign:8bhr9mqlacrewtmq2skj"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.phrase").value("New Campaign"));
    }

    @Test
    void testPutCampaign() throws Exception {
        Campaign updatedCampaign = new Campaign();
        updatedCampaign.setId("campaign:8bhr9mqlacrewtmq2skj");
        updatedCampaign.setPhrase("Updated Campaign");
        updatedCampaign.setStartDate("2024-02-20T13:55:12.7315861-03:00");
        updatedCampaign.setEndDate("2024-08-20T13:55:12.7315861-03:00");

        when(campaignService.update("campaign:8bhr9mqlacrewtmq2skj", updatedCampaign)).thenReturn(updatedCampaign);

        String requestJson = """
            {
                "id": "campaign:8bhr9mqlacrewtmq2skj",
                "phrase": "Updated Campaign",
                "startDate": "2024-02-20T13:55:12.7315861-03:00",
                "endDate": "2024-08-20T13:55:12.7315861-03:00"
            }
            """;

        mockMvc.perform(
                MockMvcRequestBuilders.put("/campaigns/campaign:8bhr9mqlacrewtmq2skj")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(requestJson)
            )
            .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    void estGetActives() throws Exception {
        Campaign activeCampaign1 = new Campaign();
        activeCampaign1.setId("campaign:8bhr9mqlacrewtmq2skj");
        activeCampaign1.setPhrase("Active Campaign 1");

        Campaign activeCampaign2 = new Campaign();
        activeCampaign2.setId("campaign:8bhr9mqlacrewtmq2sk2");
        activeCampaign2.setPhrase("Active Campaign 2");

        when(campaignService.findByCurrentDateBetweenStartDateAndEndDate()).thenReturn(Arrays.asList(activeCampaign1, activeCampaign2));

        mockMvc.perform(MockMvcRequestBuilders.get("/campaigns/actives"))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value("campaign:8bhr9mqlacrewtmq2skj"))
            .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value("campaign:8bhr9mqlacrewtmq2sk2"))
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].phrase").value("Active Campaign 1"))
            .andExpect(MockMvcResultMatchers.jsonPath("$[1].phrase").value("Active Campaign 2"));
    }
}
