# livesponsors-code-challenge

## Ferramentas utilizadas:
  - Java 17;
  - Spring Boot;
  - Spring Data JPA;
  - Spring Actuator;
  - Spring WEB;
  - Spring Validation;
  - Spring Doc;
  - Spring Test;
  - Spring Validation;
  - Java-WebSocket;
  - Surrealdb Driver;
  - Postgresql;
  - Flyway;
  - Temporal SDK;
  - Mockito Core;
  - JUnit;
  - Lombok;

## Requisitos técnicos esperados:

- [x] Modelagem das entidades: Será preciso criar as entidades LiveUser, Tweet e Campaign e os relacionamentos iniciais.

- [x] Persistência de dados: Inicialmente, utilizar o PostgreSQL como banco de dados e integrá-lo ao Spring Boot usando o Spring Data JPA. Preferível neste momento utilizar MethodQuery para facilitar a escrita das consultas.

- [x] Efetuar CRUD de entidades utilizando o SurrealDB como base de dados de LiveUser, Tweet e Campaign.

- [x] Lógica de negócios: Implementar a lógica para verificar se um tweet contém a frase de campanha e atribuir os pontos ao LiveUser correspondente.

- [x] API REST: Criar endpoints REST para listar os pontos de cada usuário e para corrigir frases de campanha no passado.

- [x] Integração com SurrealDB: Integre o SurrealDB para persistência de dados em NoSQL.

- [x] Integração com Temporal.io: Integrar para que haja orquestração de processos conforme as restrições técnicas.

- [x] Testes: Escrever testes unitários e de integração para os pontos principais da aplicação.

- [x] Documentação da API: Utilize o OpenAPI para documentar a API REST.

- [x] Diagrama da solução: Desenhar um diagrama mostrando a arquitetura geral da solução.

- [ ] Autenticação: Implementar a autenticação usando um service provider, o Spring Security atende para este caso.

- [ ] Interface visual em React/Next.js: Implementar uma interface visual que utilize React/Next.js para telas principais.

## Open API:
- http://localhost:8080/swagger-ui/index.html#/


## Temporal.io

Foi utilizado no projeto para atualizar tweet e suas pontuações após uma alteração na campanha.

### Iniciar o Temporal Server:
```
C:\Users\matheus.sobrinho\Temporal-io\temporal.exe server start-dev
```

Acessar a homepage:
```
http://localhost:8233/namespaces/default/workflows
```

## SurrealDB

Utilizado para a camada de persitência orientada a Grafo.

![Estrutura em Grafo](docs/image.png)

Start the server:
```
C:\Users\matheus.sobrinho\SurrealDB\surreal.exe start --log debug --user root --pass root  file://my-testing-directory
```

CLI

```
C:\Users\matheus.sobrinho\SurrealDB\surreal.exe sql --endpoint http://localhost:8000 --username root --password root --namespace test --database test --pretty
```

### Link e documentações:

- Surreal Docs:
  - https://surrealdb.com/docs
  - https://surrealdb.com/docs/surrealdb/introduction/start

- Temporal Docs:
  - https://docs.temporal.io/